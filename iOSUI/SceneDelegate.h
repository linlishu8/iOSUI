//
//  SceneDelegate.h
//  iOSUI
//
//  Created by 刘伟 on 2023/8/9.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

